# Git best practices

just a loose collection of techniques when using git...

[[_TOC_]]

## Better git log

get a better output for the `git log` command by introducing `git lg`:

```
git config --global alias.lg  "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
```

Alternative way: put this in your `~/.gitconfig` file:

```
[alias]
lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```

## Remove accidentally *staged* files

**Task**: you accidentally added files to the stage, which you would like to undo.

```
git restore --staged <filename>   # a single file
git restore --staged .            # all of them
```

## Remove accidentally *commited* files

**Task**: you commited now (or a while ago) certain files which should not be part of your project. You still want to keep the file locally.

```
git rm --cached <file>
git commit -m'removed file
```

## Undo last `git --amend`

Taken from https://stackoverflow.com/questions/38001038/how-to-undo-a-git-commit-amend

```bash
git reset --soft @{1}
```

`@{1}` refers to `currentbranch@{1}`, which means «where currentbranch pointed one step ago»


## Remove accidentally commited passwords

**Task**: you accidentally pushed some credentials in a file to a repository. You first want to change the password anyway, just in case. Then you want to clean up the whole git history. Finally, you would like to add the file to `git secret`.

See also: https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository

(see link above how to remove huge datafiles that should not be in the repo)

1. If you still need the credentials in a file, move it to a separate location

2. Remove the file from git and all commits in the past (this can take a while)
```bash
$ git filter-branch --force --index-filter \
  "git rm --cached --ignore-unmatch PATH-TO-YOUR-FILE-WITH-SENSITIVE-DATA" \
  --prune-empty --tag-name-filter cat -- --all
```

3. move the password-file back to the location and include it your git secret
```bash
$ git secret add "YOUR-FILE-WITH-SENSITIVE-DATA"
$ git secret tell "YOUR GPG mail address"
$ git add .gitignore
$ git add .gitsecret/paths/mapping.cfg 
$ git commit -m "Add YOUR-FILE-WITH-SENSITIVE-DATA to .gitignore"
```

4. force push your changes to the gitlab repo. If this fails, set the settings as shown here: 
https://stackoverflow.com/questions/42073357/remote-gitlab-you-are-not-allowed-to-push-code-to-protected-branches-on-this-p#53808701

```bash
$ git push origin --force --all
```

5. collaborators then should rebase (not merge!) the branches they built on top of your tainted repo

## dealing with `git secret` problems

* make sure all your colleagues have the right gpg keys
* `git secret reveal -f`
* `git secret hide -v -m`


## force merge one branch into another (kudos Geri)

**Task**: put contents branch `dev2` into branch `dev`, regardless what is in there.

Meaning: make `dev` looking identical as `dev2`, then push it to the repository

```bash
git checkout dev
git reset --hard dev2
git push -f
```

## merge several commits into one

**Task**:  two (small) commits in the past should actually be one (bigger) commit. For git this means you are cleaning up the history, the current situation is based on a newer history. Hence: `rebase`.

**Why the hell would you want to do this?**

Because when *you* work with *others* and you would like to do a merge of *your code changes* into *their* code, you better hand  it to them in a clean way, i.e. few commits, clear and concise changes. This means for you: you need to clean up your history, remove all your try-and-errors. It should look like you did all your many changes in one  go and straight-forward (which is obviously never the case). The cleaner your commits look like, the more willing are the *others* to merge *your* code into *theirs*.

- start an interactive rebase session of the last 2 commits (`HEAD~2`)

```bash
git rebase -i HEAD~2            # -i == --interactive
```

- (instead of HEAD~2 you can pick any commit hash instead)
- this opens an editor (e.g. vim) like this

```bash
pick e1536fd removed blu
pick 12f1a1e re-added blu

# Rebase 3121b24..12f1a1e onto 3121b24 (2 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup <commit> = like "squash", but discard this commit's log message
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
# Note that empty commits are commented out
```

- now replace the commit(s) you would like to combine by replacing `pick` with `squash` or simply `s`:

```bash
pick e1536fd removed blu
squash 12f1a1e re-added blu
```

- all `squash`ed commits will be merged in the commit **above** it: the top commit cannot be `squash`ed
- remove certain commits to completely: delete line or replace `pick` with `drop`
- when done: ESC !wq ENTER
- during this rebase process, CONFLICTS my occur. Resolve them by editing the relevant files and decide which portion of the code to keep
- `git add filename` for every file that is resolved
- `git rebase --continue` once all conflicts have ben resolved, to finish the rebase process
- once you see `Successfully rebased and updated refs/heads/master.` you mastered rebase. Congratulations!

## avoid `git pull`: investigate diff with repository first

From https://stackoverflow.com/questions/11935633/git-diff-between-a-remote-and-local-repository

To compare a local working directory against a remote branch, for example `origin/master`:

1. `git fetch origin master`

This tells git to fetch the branch named 'master' from the remote named 'origin'.  `git fetch` will not affect the files in your working directory; it does not try to merge changes like git pull does.

2. `git diff --summary FETCH_HEAD`

When the remote branch is fetched, it can be referenced locally via `FETCH_HEAD`. The command above tells git to diff the working directory files against FETCHed branch's HEAD and report the results in summary format. Summary format gives an overview of the changes, usually a good way to start. If you want a bit more info, use `--stat` instead of `--summary`.

3. `git diff FETCH_HEAD -- mydir/myfile.js`

If you want to see changes to a specific file, for `example myfile.js`, skip the `--summary` option and reference the file you want (or tree).


# Gitlab best practices

## Make use of gitlab access tokens and environment variables

**Task:** You have a **project A** that uses a private and not publicly available **project B**. In a CI/CD pipeline you need to checkout that project B, but of course you don't want to enter your credentials every time the pipeline runs. This is where access tokens come into place. 

### Create Access token

1. In **project B**, go to Settings -> Access Tokens
2. give token a name, and check `[x] read_repository` (for pulling)
3. create the token and copy its value

### Create Environment Variables

4. go back to **project A**
5. got to Settings -> CI/CD -> Variables
6. add variable `PROJECT_B_USERNAME` and give it a value (e.g. the username you use to access the project)
7. add variable `PROJECT_B_ACCESS_TOKEN` and paste the value of the access token (see step #3 above)

### Add environment variables to `.gitlab-ci.yml`

In your `.gitlab-ci.yml` you then can use the environment variables using the `${VARIABLE_NAME}` syntax:

```
pylint:
  stage: clean
  when: on_success
  image: python:3
  before_script:
    - pip install pylint lark py2neo bs4 lxml Flask-RESTful
  script:
    # Install dependencies (pylint import-error):
    - git clone https://${PROJECT_B_USERNAME}:${PROJECT_B_ACCESS_TOKEN}@sissource.ethz.ch/sis/private-project-b.git
    - pip install -e private-project-b
    - pylint --rcfile=pyproject.toml src/ tests/
```

After commiting `.gitlab-ci.yml`, the CI/CD runner will start automatically and use the environment variables.

## Access external repository (e.g. github) via proxy server

edit / create `.ssh/config`

```
Host github.com
	ProxyCommand /bin/nc -X connect -x proxy.ethz.ch:3128 %h %p
        Port 22
```

This enables git to pull/push from and to github.com via ssh. The `nc` command (NetCat), see https://www.computerhope.com/unix/nc.htm
`%h` and `%p` are placeholders for username/password
